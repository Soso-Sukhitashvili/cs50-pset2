#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>

bool IsLower(char item);
bool IsUpper(char item);
void EncryptAndPrint(char item, int k);

string key;

int main(int argc, string argv[])
{
    if(argc == 2)
    {

        key = argv[1];
        printf("Type text to encrypt: \n");

        string str = get_string();

        printf("ciphertext: ");

        int letterNum = 0;
        for(int i = 0, n = strlen(str); i < n; i++)
        {
            if(isalpha(str[i]))
            {
                EncryptAndPrint(str[i], letterNum);
                letterNum += 1;
            }

            else
            {
                printf("%c", str[i]);
            }
        }

        printf("\n");

        return 0;
    }
    else
    {
        return 1;
    }

}

bool IsLower(char item)
{
    if(item >= 97 && item <= 122)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool IsUpper(char item)
{
    if(item >= 65 && item <= 90){
        return true;
    }
    else{
        return false;
    }
}

void EncryptAndPrint(char item, int k)
{
    int n = strlen(key);
    int nCircled = k % n;

    if(IsLower(item) && IsLower(key[nCircled]))
    {
        char newItem = (((item - 97 + key[nCircled] - 97)) % 26) + 97;
        printf("%c", newItem);
    }
    else if(IsUpper(item) && IsUpper(key[nCircled]))
    {
        char newItem = (((item - 65 + key[nCircled] - 65)) % 26) + 65;
        printf("%c", newItem);
    }
    else
    {
        int j = IsUpper(item)? 65 : 97;
        int a = isdigit(key[nCircled])? 48 :  IsUpper(key[nCircled])? 65 : 97;
        char newItem = (((item - j + key[nCircled] - a)) % 26) + j;
        printf("%c", newItem);
    }

}