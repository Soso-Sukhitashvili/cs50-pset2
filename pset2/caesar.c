#include <stdio.h>
#include <cs50.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

// Function declarations

bool IsLower(char item);
bool IsUpper(char item);
void Encrypt(char item, int k);

int main(int argc, string argv[])
{
    //Check if argc equals 2

    if(argc == 2)
    {
        int k = atoi(argv[1]);

        printf("Type text to encrypt: \n");
        string str = get_string();

        printf("ciphertext: ");
        for(int i = 0, n = strlen(str); i < n; i++)
        {
            Encrypt(str[i], k);
        }
        printf("\n");
        return 0;
    }
    else
    {
        return 1;
    }

}

bool IsLower(char item)
{
    if(item >= 97 && item <= 122){
        return true;
    }
    else{
        return false;
    }
}

bool IsUpper(char item)
{
    if(item >= 65 && item <= 90){
        return true;
    }
    else{
        return false;
    }
}

void Encrypt(char item, int k)
{
    if(IsLower(item))
    {
        char newItem = ((item - 97 + k) % 26) + 97;
        printf("%c", newItem);
    }
    else if(IsUpper(item))
    {
        char newItem = ((item - 65 + k) % 26) + 65;
        printf("%c", newItem);
    }
    else{
        printf("%c", item);
    }
}
